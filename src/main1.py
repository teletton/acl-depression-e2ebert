from datasets import load_dataset
import numpy as np

# Let's load one of the text-classification data sets (https://huggingface.co/datasets)
data = load_dataset("poem_sentiment")
print(data.keys())
unique_labels = len(set(data['train']['label']))
all_texts = data['train']['verse_text']

print(f"Num all texts: {len(all_texts)}")
average_text_len = np.mean([len(x.split(" ")) for x in all_texts])
print(f"Approximate average text len (tokens): {average_text_len}")


from transformers import AutoTokenizer
from transformers import DataCollatorWithPadding
from transformers import AutoModelForSequenceClassification
from transformers import TrainingArguments, Trainer

# Define the tokenizer
tokenizer = AutoTokenizer.from_pretrained("distilbert-base-uncased")

# Pre-processing includes truncation
def preprocess_function(examples):
    return tokenizer(examples["verse_text"], truncation=True)

# Obtain tokenized documents
print("SEGA obicna")
print(data["train"])
tokenized_data = data.map(preprocess_function, batched=True)
print("SEGA vtorata")
print(tokenized_data["train"])
# Enable padding
data_collator = DataCollatorWithPadding(tokenizer=tokenizer)

# Define the model
model = AutoModelForSequenceClassification.from_pretrained("distilbert-base-uncased", num_labels=unique_labels)

# Train arguments' specification
training_args = TrainingArguments(
    output_dir='./results',
    learning_rate=2e-4,
    per_device_train_batch_size=32,
    per_device_eval_batch_size=32,
    num_train_epochs=1,
    weight_decay=0.01,
)

# Trainer object initialization
trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=tokenized_data["train"],
    tokenizer=tokenizer,
    data_collator=data_collator,
)

# Train the model (fine-tune, actually)
trainer.train()

from datasets import load_metric
from collections import Counter

# Let's obtain the predictions first
print(tokenized_data['test'])
print(trainer.predict(tokenized_data["test"]))
print(trainer.predict(tokenized_data["test"])[0])
model_predictions = np.argmax(trainer.predict(tokenized_data["test"])[0], axis=1)
print(model_predictions)
# Accuracy is in-built with HFTransformers
metric = load_metric("accuracy")
final_score = metric.compute(predictions=model_predictions, references=tokenized_data["test"]['label'])
print(f"Final test accuracy: {final_score}")


# Did fine-tuning actually work?
train_labels = tokenized_data['train']['label']
counter_labels_train = Counter(train_labels)
most_frequent_label = counter_labels_train.most_common(1)[0][0]

# Predict "2" for each test instance
model_predictions = np.repeat(most_frequent_label, len(model_predictions))
majority_score = metric.compute(predictions=model_predictions, references=tokenized_data["test"]['label'])
print(f"Majority classifier (label=2): {majority_score}")